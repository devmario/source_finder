#!/usr/bin/env bash

function appname {
name=$(osascript -e 'tell application "System Events" to get name of application processes whose frontmost is true and visible is true')
}

function start {
where="$1" ftype="$3" finder="$2" osascript <<END
on run argv
    tell application "iTerm"
        activate
        set win to (create window with profile "source_finder")
        tell current session of win
            write text "cd ${where} && ftype=${ftype} ${finder}"
        end tell
    end tell
end run
END
}

appname

if [ "$name" == "Xcode" ]; then
    start $1 $2 $3
fi
