# Xcode에서 소스코드 퍼지검색 하기 #

### 준비 ###
* fzf 설치
* bat 설치
* ripgrep 설치
* iTerm 설치
* [iTerm > preference > profile]에 source_finder로 된이름의 profile생성
* [source_finder/ContentView.swift > ContentView > State > from, project] 경로 설정
* 단축키설정: [source_finder/ContentView.swift > ContentView > State > shortcut]
* 그외설정: [source_finder/ContentView.swift > ContentView > State > @Published] 변수설정

### 사용법 ###
* xcode, iTerm 실행
* source_finder앱 실행시킨 상태에서 Ctrol+f
* where 폴더에서부터 검색시작
* 항목 선택하면 해당라인으로 이동
