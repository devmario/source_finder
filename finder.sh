#!/usr/bin/env bash

function jump {
line="$1"
osascript <<END
tell application "Xcode"
    activate
    tell application "System Events"
        keystroke "l" using {command down}
        keystroke ${line}
        keystroke return
    end tell
end tell
END
}

function close {
osascript <<END
tell application "iTerm"
    tell current session of current window
        write text "exit"
    end tell
end tell
END
}

function finder {
IFS=: read -ra selected < <(
rg -g "${ftype}" --color=always --line-number --no-heading --smart-case --trim "${*:-}" |
fzf --ansi \
    --color "hl:-1:underline,hl+:-1:underline:reverse" \
    --info "hidden" \
    --delimiter : \
    --preview 'bat --color=always {1} --highlight-line {2}' \
    --preview-window 'up,60%,border-bottom,+{2}+3/3,~3'
)
}

finder

if [ -n "${selected[0]}" ]; then
    open $(pwd)/${selected}
    jump ${selected[1]}
else
    echo "not found"
fi

close

