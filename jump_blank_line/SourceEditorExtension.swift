//
//  SourceEditorExtension.swift
//  jump_blank_line
//
//  Created by wonhee jang on 2021/10/15.
//

import Foundation
import XcodeKit

class SourceEditorExtension: NSObject, XCSourceEditorExtension {
    /*
    func extensionDidFinishLaunching() {
        // If your extension needs to do any work at launch, implement this optional method.
    }
    */
    
    var commandDefinitions: [[XCSourceEditorCommandDefinitionKey: Any]] {
        let identifierPrefix = "breezm.source-finder.jump-blank-line.SourceEditorCommand."
        return ["previous", "next"].map { name in
            var dict = [XCSourceEditorCommandDefinitionKey: String]()
            dict[.classNameKey] = SourceEditorCommand.className()
            dict[.identifierKey] = identifierPrefix + name
            dict[.nameKey] = name
            return dict as [XCSourceEditorCommandDefinitionKey: Any]
        }
    }
    
}
