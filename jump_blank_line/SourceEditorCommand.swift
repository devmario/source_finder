//
//  SourceEditorCommand.swift
//  jump_blank_line
//
//  Created by wonhee jang on 2021/10/15.
//

import Foundation
import XcodeKit

class SourceEditorCommand: NSObject, XCSourceEditorCommand {
    func jump(invocation: XCSourceEditorCommandInvocation, i: Int) {
        let position = XCSourceTextPosition(line: i, column: 0)
        invocation.buffer.selections.setArray([XCSourceTextRange(start: position, end: position)])
    }
    
    var started = true
    func jumpAtEmptyLine(invocation: XCSourceEditorCommandInvocation, i: Int) -> Bool {
        guard let line = invocation.buffer.lines[i] as? String else { return false }
        
        if line.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            if started {
                return false
            }
            jump(invocation: invocation, i: i)
            return true
        }
        started = false
        return false
    }
    
    func perform(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void ) -> Void {
        defer { completionHandler(nil) }
        
        guard let firstSelection = invocation.buffer.selections.firstObject as? XCSourceTextRange,
              let lastSelection = invocation.buffer.selections.lastObject as? XCSourceTextRange else { return }
        
        guard let commandName = invocation.commandIdentifier.components(separatedBy: ".").last else { return }
        
        if commandName == "previous" {
            if firstSelection.start.line == 0 {
                return
            }
            
            started = true
            for i in (0..<firstSelection.start.line).reversed() {
                if jumpAtEmptyLine(invocation: invocation, i: i) {
                    return
                }
            }
            
            jump(invocation: invocation, i: 0)
        } else if commandName == "next" {
            if invocation.buffer.lines.count == lastSelection.end.line {
                return
            }
            
            started = true
            for i in (lastSelection.end.line..<invocation.buffer.lines.count) {
                if jumpAtEmptyLine(invocation: invocation, i: i) {
                    return
                }
            }
            
            jump(invocation: invocation, i: invocation.buffer.lines.count)
        }
    }
    
}
