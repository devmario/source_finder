//
//  ContentView.swift
//  source_finder
//
//  Created by wonhee jang on 2021/10/13.
//

import SwiftUI
import MASShortcut

protocol CommandExecuting {
    func run(commandName: String, arguments: [String]) throws -> String
}

enum BashError: Error {
    case commandNotFound(name: String)
}

struct Bash: CommandExecuting {
    func run(commandName: String, arguments: [String] = []) throws -> String {
        return try run(resolve(commandName), with: arguments)
    }
    
    private func resolve(_ command: String) throws -> String {
        guard var bashCommand = try? run("/bin/bash" , with: ["-l", "-c", "which \(command)"]) else {
            throw BashError.commandNotFound(name: command)
        }
        bashCommand = bashCommand.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return bashCommand
    }
    
    private func run(_ command: String, with arguments: [String] = []) throws -> String {
        let process = Process()
        process.launchPath = command
        process.arguments = arguments
        let outputPipe = Pipe()
        process.standardOutput = outputPipe
        process.launch()
        let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(decoding: outputData, as: UTF8.self)
        return output
    }
}

struct ContentView: View {
    class State: ObservableObject {
        static let `default` = State()
        
        let shortcut = MASShortcut(keyCode: kVK_ANSI_F, modifierFlags: .control) // shortcut
        
        init() {
            MASShortcutMonitor.shared().register(shortcut) {
                self.action()
            }
        }
        
        deinit {
            MASShortcutMonitor.shared().unregisterShortcut(shortcut)
        }
        
        static let from = "/Users/wonheejang/Documents" // for absolute path
        static let project = "\(from)/source_finder" // source_finder project path
        
        @Published var start = "\(project)/start.sh" // start up script path
        @Published var `where` = "\(from)/breezmAppleApp/" // starting location for search
        @Published var finder = "\(project)/finder.sh" // finder script path
        @Published var ftype = "*.{swift}" // file type (*.{c,m,h,cc,hh,cpp,hpp,json,...})
        
        func action() {
            let bash: CommandExecuting = Bash()
            if let _ = try? bash.run(commandName: self.start, arguments: [
                self.where,
                self.finder,
                self.ftype
            ]) {
            }
        }
    }
    
    @StateObject var state = State.default
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            VStack(alignment: .leading) {
                Text("where")
                TextField("where", text: $state.where)
            }
            VStack(alignment: .leading) {
                Text("ftype")
                TextField("ftype", text: $state.ftype)
            }
            
            Divider()
            
            VStack(alignment: .leading) {
                Text("start")
                TextField("start", text: $state.start)
            }
            VStack(alignment: .leading) {
                Text("finder")
                TextField("finder", text: $state.finder)
            }
        }
        .fixedSize()
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
