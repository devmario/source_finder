//
//  SourceFinderApp.swift
//  SourceFinderApp
//
//  Created by wonhee jang on 2021/10/13.
//

import SwiftUI

@main
struct SourceFinderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
